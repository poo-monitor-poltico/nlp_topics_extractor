import pandas as pd
import string
import re
import nltk

nltk.download('stopwords')
stopwords = nltk.corpus.stopwords.words('portuguese')
default_data_filename='./data/tabela.csv'


def getDataframe(data_filename=default_data_filename):
    dataframe = pd.read_csv(data_filename)
    
    return dataframe

def cleanData(dataframe):
    remove_data = dataframe[dataframe['indexacao'].isna()].index.tolist()
    dataframe = dataframe.drop(remove_data)
    clean_dataframe = dataframe.reset_index(drop = True)
    
    return clean_dataframe
    
def removePunctuation(text):
    text_nopunct = "".join([char for char in text if char not in string.punctuation])
    return text_nopunct

def tokenize(text):
    tockens = re.split('\W+', text)
    return tockens

def removeStopwords(tockenized_list):
    text = [word for word in tockenized_list if word not in stopwords or len(word) > 4]
    return text

def preprocesText(dataframe):
    dataframe['indexacao'] = dataframe['indexacao'].apply(lambda x: removePunctuation(x))
    dataframe['indexacao'] = dataframe['indexacao'].apply(lambda x: tokenize(x.lower()))
    dataframe['indexacao'] = dataframe['indexacao'].apply(lambda x: removeStopwords(x))
    return dataframe
    
def preProcessing():
    
    dataframe = getDataframe(default_data_filename)
    clean_dataframe = cleanData(dataframe)
    dataframe = preprocesText(clean_dataframe)
    return dataframe
    
    